FROM openjdk:8-jdk

WORKDIR /opt

ADD target/stackoverflow-api-questions-fat.jar /opt/target/stackoverflow-api-questions-fat.jar
ADD target/classes/configuration/docker-config.json /opt/target/configuration/docker-config.json

RUN chmod a+rwx target/stackoverflow-api-questions-fat.jar

EXPOSE 80

CMD ["java", "-jar", "target/stackoverflow-api-questions-fat.jar", "-conf", "target/configuration/docker-config.json"]