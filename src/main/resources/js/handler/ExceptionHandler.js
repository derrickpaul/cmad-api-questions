/**
 * Handler to catch and handle any exception or forced failure from any of the
 * API handlers. This handler will return corresponding status code and end the
 * response.
 */
var ExceptionHandler = {
    logger : Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.ExceptionHandler"),
    handle : function(rc) {
        var statusCode = -1;
        var statusMessage = "Unexpected internal error";

        // Use status code from context, if its explicitly set.
        if (rc.statusCode() != null && rc.statusCode() > 0) {
            statusCode = rc.statusCode();
        }

        // Log the exception.
        ExceptionHandler.logger.error("Exception received. Status code: " + rc.statusCode() + ". Request: " + rc.request().method()
                + " " + rc.request().uri(), (rc.failure() != null) ? rc.failure() : "Cause unknown");

        if (rc.failure() != null) {
            // Set status code based on exception thrown.
            try {
                if (rc.failure().getCause() != null && rc.failure().getCause().failureCode() != null) {
                    statusCode = rc.failure().getCause().failureCode();
                    statusMessage = rc.failure().getCause().getMessage();
                }
            } catch (e) {
                ExceptionHandler.logger.error("Unexpected error in exception handling.");
            }
        }

        if (statusCode == -1) {
            statusCode = 500;
        }

        rc.response().setStatusCode(parseInt(statusCode)).setStatusMessage(statusMessage).end();
    }
}