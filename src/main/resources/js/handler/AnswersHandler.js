/**
 * Handles operations related to answers.
 */
var AnswersHandler = {
    logger : Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.handler.AnswersHandler"),
    postAnswer : function(rc) {
    	AnswersHandler.logger.debug("Posting answer...");
    	var input = rc.getBodyAsJson();
		// publish an event on 'com.cisco.cmad.so.answers.post' with answer json. 
		
        var answer = {};

        // Validate input attributes and populate domain model.
        if (input == null) {
            rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
            return;
        }

        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
        	answer.text = input.text;
        } else {
            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
            return;
        }

        // Populate user and meta-data attributes.
        answer.userId = rc.user().principal().sub;
        answer.userName = rc.user().principal().lastName + ", " + rc.user().principal().firstName;

        answer.createdTime = (new Date()).getTime();
        answer.voteCount = 0;
        answer.questionId = rc.request().getParam("questionId"); 

        AnswersHandler.logger.debug("Send event to java handler...")
        vertx.eventBus().send("com.cisco.cmad.so.answers.post", answer, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(201).putHeader("location", result.body()).end();
            }
        });
    },
    updateAnswer : function(rc) {
    	AnswersHandler.logger.debug("Patching answer...");
    	
        var answer = {};

        answer.lastUpdatedUserId = rc.user().principal().sub;
        answer.lastUpdatedUserName = rc.user().principal().lastName + ", " + rc.user().principal().firstName;
        answer.lastUpdatedTime = (new Date()).getTime();
        answer.answerId = rc.request().getParam("answerId");
        
        var action = rc.request().getParam("action");
        var event = "com.cisco.cmad.so.answers.update";
        
        if (!action || action == 'CONTENT_UPDATE') {
        	var input = rc.getBodyAsJson();

        	if (input == null) {
        		rc.response().setStatusCode(422).setStatusMessage("Empty request").end();
        		return;
        	}
        	
	        if (input.text && typeof input.text === 'string' && input.text.trim().length > 0) {
	        	answer.text = input.text;
	        } else {
	            rc.response().setStatusCode(422).setStatusMessage("Attribute 'text' is empty.").end();
	            return;
	        }
        } else if (action && action == 'VOTE_UP') {
        	event = "com.cisco.cmad.so.answers.vote";
        	answer.vote = 1;
        } else if (action && action == 'VOTE_DOWN') {
        	event = "com.cisco.cmad.so.answers.vote";
        	answer.vote = -1;
        } else if (action && action == "SET_ANSWER") {
        	
        }

        AnswersHandler.logger.debug("Send event to java handler...")
        vertx.eventBus().send(event, answer, function(result, error) {
            if (error) {
                rc.fail(error);
            } else {
                rc.response().setStatusCode(200).end();
            }
        });
    }
    
    
}