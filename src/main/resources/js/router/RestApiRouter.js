/**
 * Holds all the REST API route definitions.
 */

var RestApiRouter = {
    logger : Java.type("io.vertx.core.logging.LoggerFactory").getLogger("js.router.RestApiRouter"),
    router : null,

    create : function() {
        var Router = require("vertx-web-js/router");
        this.router = Router.router(vertx);

        RestApiRouter.logger.info("Configuring routes for all REST API.");

        load("classpath:js/handler/QuestionsHandler.js");
        load("classpath:js/handler/AnswersHandler.js");
        load("classpath:js/handler/CommentsHandler.js");

        // Questions handlers
        this.router.post("/questions").handler(QuestionsHandler.postQuestion);
        this.router.get("/questions").handler(QuestionsHandler.searchQuestions);
        this.router.get("/questions/tags").handler(QuestionsHandler.searchTagsByName);
        this.router.get("/questions/:questionId").handler(QuestionsHandler.getQuestionById);
        this.router.patch("/questions/:questionId").handler(QuestionsHandler.updateQuestion);
        this.router.put("/questions/:questionId").handler(QuestionsHandler.updateQuestion);

        // Answers handlers
        this.router.post("/questions/:questionId/answers").handler(AnswersHandler.postAnswer);
        this.router.patch("/questions/answers/:answerId").handler(AnswersHandler.updateAnswer);
        this.router.put("/questions/answers/:answerId").handler(AnswersHandler.updateAnswer);

        // Comments handlers
        this.router.post("/questions/:questionId/comments").handler(CommentsHandler.postCommentOnQuestion);
        this.router.patch("/questions/comments/:commentId").handler(CommentsHandler.updateCommentOnQuestion);
        this.router.put("/questions/comments/:commentId").handler(CommentsHandler.updateCommentOnQuestion);
        this.router.post("/questions/answers/:answerId/comments").handler(CommentsHandler.postCommentOnAnswer);
        this.router.patch("/questions/answers/comments/:commentId").handler(CommentsHandler.updateCommentOnAnswer);
        this.router.put("/questions/answers/comments/:commentId").handler(CommentsHandler.updateCommentOnAnswer);

        return this;
    }
}