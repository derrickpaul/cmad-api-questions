package com.cisco.cmad.so.util;

import com.cisco.cmad.so.service.QuestionsService;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.MongoClient;

public class MongoUtil {

    private static Logger logger = LoggerFactory.getLogger(MongoUtil.class.getName());

    public static final String ENV_MONGODB_DATABASE = "SO_MONGODB_DATABASE";

    public static final String ENV_MONGODB_URL = "SO_MONGODB_URL";

    private static final String DEFAULT_MONGODB_DATABASE = "cmad";

    private static final String DEFAULT_MONGODB_URL = "mongodb://cmad-mongodb:27017";

    private static MongoClient client = null;

    private MongoUtil() {
    }

    public static synchronized void initialize(Vertx vertx) {
        if (client != null)
            return;

        logger.info("Initializing mongodb client");

        String mongoDbName = DEFAULT_MONGODB_DATABASE;
        String mongoDbUrl = DEFAULT_MONGODB_URL;

        // Over-ride defaults from Environment variable, if available.
        String envMongoDbName = System.getenv(ENV_MONGODB_DATABASE);
        if (envMongoDbName != null && !envMongoDbName.isEmpty()) {
            logger.info(
                    "Found and using environment variable " + ENV_MONGODB_DATABASE + " with value " + envMongoDbName);
            mongoDbName = envMongoDbName;
        } else {
            logger.info("Using default mongodb database : " + DEFAULT_MONGODB_DATABASE);
        }

        String envMongoDbUrl = System.getenv(ENV_MONGODB_URL);
        if (envMongoDbUrl != null && !envMongoDbUrl.isEmpty()) {
            logger.info("Found and using environment variable " + ENV_MONGODB_URL);
            mongoDbUrl = envMongoDbUrl;
        } else {
            logger.info("Using default mongodb url : " + DEFAULT_MONGODB_URL);
        }

        JsonObject config = new JsonObject();
        config.put("db_name", mongoDbName);
        config.put("connection_string", mongoDbUrl);
        client = MongoClient.createShared(vertx, config);

        // Create indexes on the MongoDB instance.
        initializeIndices();
    }

    public static MongoClient getMongoClient() {
        return client;
    }

    public static boolean isInitialized() {
        return client != null;
    }

    private static void initializeIndices() {

        // Indexes for Questions collection.
        logger.info("Initializing indices for questions collection");
        
        // INDEX 1: Text index on title and content fields.
        JsonObject textSearchIndex = new JsonObject();
        textSearchIndex.put("title", "text");
        textSearchIndex.put("text", "text");
        getMongoClient().createIndex(QuestionsService.QUESTIONS, textSearchIndex, result -> {
            if (result.failed()) {
                logger.error("Error creating text search index on questions.", result.cause());
            }
        });
        
        // INDEX 2: Index on the 'tags' array
        JsonObject multiKeyIndex = new JsonObject();
        multiKeyIndex.put("tags", 1);
        getMongoClient().createIndex(QuestionsService.QUESTIONS, multiKeyIndex, result -> {
            if (result.failed()) {
                logger.error("Error creating index on questions.tags attribute.", result.cause());
            }
        });
    }
}
