package com.cisco.cmad.so.service;

import java.util.Date;
import java.util.UUID;

import com.cisco.cmad.so.util.MongoUtil;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class CommentsService {
	private static Logger logger = LoggerFactory.getLogger(CommentsService.class.getName());
	
	public void postCommentOnQuestion(Message<Object> message) {
		logger.debug("Posting comment on question to database...");

		String commentId = UUID.randomUUID().toString();
		JsonObject comment = new JsonObject(message.body().toString()).put("commentId", commentId);
		String questionId = (String) comment.remove("entityId");

		JsonObject update = new JsonObject().put("$addToSet", new JsonObject().put("comments", comment));
		update.put("$set", new JsonObject().put("lastUpdatedTime", new Date().getTime()));
		JsonObject query = new JsonObject().put("_id", questionId);

		System.out.println("Query : " + query);
		System.out.println("Update : " + update);

		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				
				message.reply(commentId);
			} else {
				logger.error("Error updating answer in database", reply.cause());
				message.fail(500, "Database error.");
			}
		});
	}

	public void updateCommentOnQuestion(Message<Object> message) {
		logger.debug("Updating comment on question in database...");

		JsonObject comment = new JsonObject(message.body().toString());
		String commentId = (String) comment.remove("commentId");

		JsonObject update = new JsonObject().put("$set", new JsonObject());
		for (String field : comment.fieldNames()) {
			update.getJsonObject("$set").put("comments.$." + field, comment.getValue(field));
		}
		
		update.getJsonObject("$set").put("lastUpdatedTime", new Date().getTime());
		
		JsonObject query = new JsonObject().put("comments.commentId", commentId);

		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				
				message.reply(commentId);
			} else {
				logger.error("Error updating comment for question in database", reply.cause());
				message.fail(500, "Database error.");
			}
		});
	}

	public void postCommentOnAnswer(Message<Object> message) {
		logger.debug("Posting comment on answer to database...");

		String commentId = UUID.randomUUID().toString();
		JsonObject comment = new JsonObject(message.body().toString()).put("commentId", commentId);
		String answerId = (String) comment.remove("entityId");

		JsonObject update = new JsonObject().put("$set", new JsonObject().put("answers.$.comments." + commentId, comment));
		update.getJsonObject("$set").put("lastUpdatedTime", new Date().getTime());
		JsonObject query = new JsonObject().put("answers.answerId", answerId);

		System.out.println("Query : " + query);
		System.out.println("Update : " + update);
		
		logger.debug(query.encodePrettily());

		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				
				message.reply(commentId);
			} else {
				logger.error("Error updating answer in database", reply.cause());
				message.fail(500, "Database error.");
			}
		});
	}

	public void updateCommentOnAnswer(Message<Object> message) {
		logger.debug("Posting comment on answer to database...");

		JsonObject comment = new JsonObject(message.body().toString());
		String commentId = (String) comment.remove("commentId");

		JsonObject update = new JsonObject().put("$set", new JsonObject().put("answers.$.comments." + commentId, comment));
		update.getJsonObject("$set").put("lastUpdatedTime", new Date().getTime());
		JsonObject query = new JsonObject().put("answers.comments." + commentId + ".commentId", commentId);

		System.out.println("Query : " + query);
		System.out.println("Update : " + update);
		
		logger.debug(query.encodePrettily());

		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				
				logger.info("updateCommentOnAnswer UpsertedId : " + reply.result().getDocUpsertedId());
				// updateQuestionTime("comments", commentId);
				
				message.reply(commentId);
			} else {
				logger.error("Error updating answer in database", reply.cause());
				message.fail(500, "Database error.");
			}
		});
	}
}
