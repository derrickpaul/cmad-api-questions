package com.cisco.cmad.so.service;

import java.util.Date;
import java.util.UUID;

import com.cisco.cmad.so.util.MongoUtil;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class AnswersService {

	private static Logger logger = LoggerFactory.getLogger(AnswersService.class.getName());
	private final static String ANSWER_ID = "answerId";
	
	public void postAnswer(Message<Object> message) {
		logger.debug("Posting answer to database...");

		String answerId = generateUUID();
		JsonObject answer = new JsonObject(message.body().toString()).put(ANSWER_ID, answerId);
		String questionId = (String) answer.remove("questionId");

		JsonObject update = new JsonObject().put("$addToSet", new JsonObject().put("answers", answer));
		update.put("$set", new JsonObject().put("lastUpdatedTime", new Date().getTime()));
		JsonObject query = new JsonObject("{ \"_id\": \"" + questionId + "\" }");

		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
			    
				message.reply(answerId);
			} else {
				logger.error("Error posting answer to database", reply.cause());
				message.fail(500, "Database error.");
			}
		});
	}

	public void updateAnswer(Message<Object> message) {
		logger.debug("Updating answer...");

		JsonObject answer = new JsonObject(message.body().toString());
		String answerId = answer.getString(ANSWER_ID);

		JsonObject update = new JsonObject().put("$set", new JsonObject());
		for (String field : answer.fieldNames()) {
			update.getJsonObject("$set").put("answers.$." + field, answer.getValue(field));
		}
		
		update.getJsonObject("$set").put("lastUpdatedTime", new Date().getTime());

		JsonObject query = new JsonObject().put("answers.answerId", answerId);

		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				
				message.reply(reply.result().toString());
			} else {
				logger.error("Error updating answer in database", reply.cause());
				message.fail(500, "Database error.");
			}
		});
	}

	public void vote(Message<Object> message) {
		logger.debug("voting on answer...");

		JsonObject answer = new JsonObject(message.body().toString());
		String answerId = answer.getString(ANSWER_ID);
		int vote = answer.getInteger("vote");

		JsonObject update = new JsonObject().put("$inc", new JsonObject().put("answers.$.voteCount", vote));
		JsonObject query = new JsonObject().put("answers.answerId", answerId);
		
		MongoUtil.getMongoClient().updateCollection("questions", query, update, reply -> {
			if (reply.succeeded()) {
				logger.debug("Matched : " + reply.result().getDocMatched());
				logger.debug("Updated : " + reply.result().getDocModified());
				
				message.reply("");
			} else {
				logger.error("Error upvoting answer in database", reply.cause());
				message.fail(500, "Database error.");
			}
		});
	}

	private String generateUUID() {
		return UUID.randomUUID().toString();
	}
	
}
