/*
 * StackOverflow Emulation - Questions APIs
 * The questions API micro-service. This is part of the StackOverflow sample use case taken to learn modern application development.
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.cisco.cmad.it.so.client.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * *Sample JSON Structure:*  1. When posted: &#x60;&#x60;&#x60;  {   text: \&quot;This is the content of a sample answer\&quot;,   tags: [\&quot;sample\&quot;]  } &#x60;&#x60;&#x60; 2. When retrieved: &#x60;&#x60;&#x60;  {   _id: \&quot;1234-5678-9012-3456\&quot;,   userId: \&quot;smithblack@stackoverflow.com\&quot;,   userName: \&quot;Smith Black\&quot;,   text: \&quot;This is the content of a sample answer\&quot;,   tags: [\&quot;sample\&quot;],   voteCount: 45,   createdTime: 1493719334210,   lastUpdatedTime: 1493719362860,   isSelectedAnswer: false,   comments: [{    _id: \&quot;1234-5678-9012-3456\&quot;,    userId: \&quot;6754-0987-6754-3289\&quot;,    userDisplayName: \&quot;John Doe\&quot;,    text: \&quot;This is the content of a sample comment\&quot;,    tags: [\&quot;sample\&quot;],    voteCount: 23,    createdTime: 1493719334210,    lastUpdatedTime: 1493719362860   }]  } &#x60;&#x60;&#x60;
 */
@ApiModel(description = "*Sample JSON Structure:*  1. When posted: ```  {   text: \"This is the content of a sample answer\",   tags: [\"sample\"]  } ``` 2. When retrieved: ```  {   _id: \"1234-5678-9012-3456\",   userId: \"smithblack@stackoverflow.com\",   userName: \"Smith Black\",   text: \"This is the content of a sample answer\",   tags: [\"sample\"],   voteCount: 45,   createdTime: 1493719334210,   lastUpdatedTime: 1493719362860,   isSelectedAnswer: false,   comments: [{    _id: \"1234-5678-9012-3456\",    userId: \"6754-0987-6754-3289\",    userDisplayName: \"John Doe\",    text: \"This is the content of a sample comment\",    tags: [\"sample\"],    voteCount: 23,    createdTime: 1493719334210,    lastUpdatedTime: 1493719362860   }]  } ```")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-07-20T11:07:30.503Z")
public class Body5 {
  @SerializedName("_id")
  private String id = null;

  @SerializedName("userId")
  private String userId = null;

  @SerializedName("userName")
  private String userName = null;

  @SerializedName("text")
  private String text = null;

  @SerializedName("tags")
  private List<String> tags = null;

  @SerializedName("voteCount")
  private Integer voteCount = null;

  @SerializedName("createdTime")
  private BigDecimal createdTime = null;

  @SerializedName("lastUpdatedTime")
  private BigDecimal lastUpdatedTime = null;

  @SerializedName("isSelectedAnswer")
  private Boolean isSelectedAnswer = null;

  @SerializedName("comments")
  private List<InlineResponse200Comments> comments = null;

  public Body5 id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Identifier
   * @return id
  **/
  @ApiModelProperty(value = "Identifier")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Body5 userId(String userId) {
    this.userId = userId;
    return this;
  }

   /**
   * The identifier of the user who entered this answer.
   * @return userId
  **/
  @ApiModelProperty(value = "The identifier of the user who entered this answer.")
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Body5 userName(String userName) {
    this.userName = userName;
    return this;
  }

   /**
   * The display name of the user.
   * @return userName
  **/
  @ApiModelProperty(value = "The display name of the user.")
  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Body5 text(String text) {
    this.text = text;
    return this;
  }

   /**
   * The answer content.
   * @return text
  **/
  @ApiModelProperty(required = true, value = "The answer content.")
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Body5 tags(List<String> tags) {
    this.tags = tags;
    return this;
  }

  public Body5 addTagsItem(String tagsItem) {
    if (this.tags == null) {
      this.tags = new ArrayList<String>();
    }
    this.tags.add(tagsItem);
    return this;
  }

   /**
   * Any tags relevant to the answer
   * @return tags
  **/
  @ApiModelProperty(value = "Any tags relevant to the answer")
  public List<String> getTags() {
    return tags;
  }

  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  public Body5 voteCount(Integer voteCount) {
    this.voteCount = voteCount;
    return this;
  }

   /**
   * Get voteCount
   * @return voteCount
  **/
  @ApiModelProperty(value = "")
  public Integer getVoteCount() {
    return voteCount;
  }

  public void setVoteCount(Integer voteCount) {
    this.voteCount = voteCount;
  }

  public Body5 createdTime(BigDecimal createdTime) {
    this.createdTime = createdTime;
    return this;
  }

   /**
   * Get createdTime
   * @return createdTime
  **/
  @ApiModelProperty(value = "")
  public BigDecimal getCreatedTime() {
    return createdTime;
  }

  public void setCreatedTime(BigDecimal createdTime) {
    this.createdTime = createdTime;
  }

  public Body5 lastUpdatedTime(BigDecimal lastUpdatedTime) {
    this.lastUpdatedTime = lastUpdatedTime;
    return this;
  }

   /**
   * Get lastUpdatedTime
   * @return lastUpdatedTime
  **/
  @ApiModelProperty(value = "")
  public BigDecimal getLastUpdatedTime() {
    return lastUpdatedTime;
  }

  public void setLastUpdatedTime(BigDecimal lastUpdatedTime) {
    this.lastUpdatedTime = lastUpdatedTime;
  }

  public Body5 isSelectedAnswer(Boolean isSelectedAnswer) {
    this.isSelectedAnswer = isSelectedAnswer;
    return this;
  }

   /**
   * Is this the selected answer?
   * @return isSelectedAnswer
  **/
  @ApiModelProperty(value = "Is this the selected answer?")
  public Boolean getIsSelectedAnswer() {
    return isSelectedAnswer;
  }

  public void setIsSelectedAnswer(Boolean isSelectedAnswer) {
    this.isSelectedAnswer = isSelectedAnswer;
  }

  public Body5 comments(List<InlineResponse200Comments> comments) {
    this.comments = comments;
    return this;
  }

  public Body5 addCommentsItem(InlineResponse200Comments commentsItem) {
    if (this.comments == null) {
      this.comments = new ArrayList<InlineResponse200Comments>();
    }
    this.comments.add(commentsItem);
    return this;
  }

   /**
   * List of comments for this answer.
   * @return comments
  **/
  @ApiModelProperty(value = "List of comments for this answer.")
  public List<InlineResponse200Comments> getComments() {
    return comments;
  }

  public void setComments(List<InlineResponse200Comments> comments) {
    this.comments = comments;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Body5 body5 = (Body5) o;
    return Objects.equals(this.id, body5.id) &&
        Objects.equals(this.userId, body5.userId) &&
        Objects.equals(this.userName, body5.userName) &&
        Objects.equals(this.text, body5.text) &&
        Objects.equals(this.tags, body5.tags) &&
        Objects.equals(this.voteCount, body5.voteCount) &&
        Objects.equals(this.createdTime, body5.createdTime) &&
        Objects.equals(this.lastUpdatedTime, body5.lastUpdatedTime) &&
        Objects.equals(this.isSelectedAnswer, body5.isSelectedAnswer) &&
        Objects.equals(this.comments, body5.comments);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, userId, userName, text, tags, voteCount, createdTime, lastUpdatedTime, isSelectedAnswer, comments);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Body5 {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    tags: ").append(toIndentedString(tags)).append("\n");
    sb.append("    voteCount: ").append(toIndentedString(voteCount)).append("\n");
    sb.append("    createdTime: ").append(toIndentedString(createdTime)).append("\n");
    sb.append("    lastUpdatedTime: ").append(toIndentedString(lastUpdatedTime)).append("\n");
    sb.append("    isSelectedAnswer: ").append(toIndentedString(isSelectedAnswer)).append("\n");
    sb.append("    comments: ").append(toIndentedString(comments)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

