#!/bin/bash

echo "Resize api-cluster to two instances"
gcloud container clusters resize api-cluster --size=2 --zone=us-central1-a